# Haberdashery

###### The node application manager

![Haberdashery](https://images.unsplash.com/photo-1542044801-30d3e45ae49a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9593fcd9959121604a9522f3422e9527&auto=format&fit=crop&w=1500&q=80)

Haberdashery is a node application manager. It simplifies local development by allowing you to run all your project applications in one terminal. Run all your node API micro-services, plus your front end by simply configuring an RC file.

Not only can you start all the apps with one command, but all the output from each app gets logged in the one terminal tab, saving you time from cycling through tabs and windows trying to find which app has crashed or where your console log went.

### Run locally
- npm install haberdashery -g
- Create an rc file
- run `haberdashery` or `dash`

#### RC File
create `.dashrc` with your default start script and an array of directories you have your apps in

In this case, I am running `npm run start` on the directories listed

```
{
    "applications": [
        {
            "directory": "~/Projects/auth-service",
            "script": "start"
        },
        {
            "directory": "~/Projects/checkout-service",
            "script": "start"
        },
        {
            "directory": "~/Projects/search-service",
            "script": "start"
        }
    ]
}
```

You can specify to use yarn by adding the following to your `.dashrc` file:

```
{
    "packageManager": "yarn"
}
```