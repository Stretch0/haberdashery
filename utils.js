const colours = require('./colours.json')

// Get terminal output colour
let i = 0
const getColour = () => {
	// Reset colour key
	if(i > colours.length - 1) i = 0
	const colour = colours[i]
	i++
	return colour
}

module.exports = {
	getColour
}