const { exec } = require('child_process')
const log = require("./log")
const colours = require('./colours.json')
const { getColour } = require("./utils")
const { applications, packageManager } = require('rc')('dash', {
  applications: [],
  packageManager: "npm"
})

const spawn = () => {

	applications.forEach(({directory, script}) => {

		const colour = getColour()

		let start = `npm run ${script} --prefix ${directory}`

		if(packageManager === "yarn"){
			start = `yarn --cwd ${directory} ${script}`			
		}
		
		// start child process
		const child = exec(start)
		
		// logs stdin stdout stderr and close events
		log({child, directory, colour})

	})
	
}

module.exports = spawn