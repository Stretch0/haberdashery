const chalk = require("chalk")

/*
 *	Listen to data event which is triggered when the
 *	child process outputs data (i.e. console logs)
 **/
const log = ({child, directory, colour}) => {
	child.stdin.on('data', (data) => {
	  const file = directory.split("/")
	  console.log(chalk.bold[colour](file[file.length -1]), data.toString())
	})
	
	child.stdout.on('data', (data) => {
	  const file = directory.split("/")
	  console.log(chalk.bold[colour](file[file.length -1]), data.toString())
	})

	child.stderr.on('data', (data) => {
	  const file = directory.split("/")
	  console.log(chalk.bold[colour](file[file.length -1]), data.toString())
	});

	child.on('close', (code) => {
		const file = directory.split("/")
		console.log(chalk.bold[colour](file[file.length -1]), `child process exited with code ${code}`)
	});
}

module.exports = log