#!/usr/bin/env node
const program = require('commander')
const spawn = require('./spawn')

program
	.action(spawn)
	.parse(process.argv);